# Notas sobre el proyecto #

Usa la arquitectura **DDD**


## common

common/ es la capa comun o transversal.

## backend

### dependencias  que no estan el el codigo

Un repositorio gitlab de adsib

* app-io:

Todos los siguientes son paquetes subidos a npm

* app-logs:
* app-params:

### src


definicion de models/ con sequelize

hay una etiqueta
**xlabel**: lang.t('fields.nombre')

que es para usar labels definidos para diferentes idiomas.

#### repositorios

Aqui van consultas a la base de datos archivos javascript.

const {  tests, entidades } = models; // manera de ECMA6
// similar a
const tests = models.tests;
const entidades = models.entidades;

En muchos casos se usa *async await* para facilitar la resolución de promesas.
En esos casos hay que usar *try, catch*.

#### hooks

Son como middlewares o interceptores, por ejemplo en el proyecto hay un hook principal para la traducción de errores a distintos idiomas.

#### seeders

Se usa la biblioteca *casual* que genera datos al azar.

#### tests

Los tests unitarios en este caso del proyecto hay tests por cada tabla, se usa *ava* para los tests.

### index.js

Obtiene la configuración del common, sequelize ,repositorios, carpetas y carga las asociaciones de los modelos

### modelos

Se definen de la forma habitual de sequelize pero en este proyecto hay un archivo a parte *associations.js* donde se definen las asociasiones.

### setup.js

Este archivo ejeucta la configuración inicial, generación de base de datos.

## Capa de controladores (services)

Cada tabla tiene sus servicios que reciben repositorios, es como una extesión de repositorios.

El orden que se maneja es modelos, repositorios y servicios. Donde servicios es para hacer tareas adicionales.

### lib

utilitarios importantes.

## Capa de aplicación (application)

index.js: carga los servicios definidos y servicios de bibliotecas externas.

### api (SERVICIOS REST)

**API REST** que tinene un index.js 

La idea es que en la capa de dominio se haga la lógica en la capa de aplicación se consuma lo que hacen los dominios.

**api.js** Se cargan las apis necesarias, aquí por ejemplo van consultas y respuestas de el módulo de interoperabilidad y otros.
Desde aquí también se envía mensajes directamente al frontend.

Al definir un endpoint se le pasa un parametro *guard* para comprobar permisos.

### graphql

Graphql, no hace consultas directamente a la BD, es una capa de filtros.

También se hace una verificación de permisos usando guard.

#### schemas

Se definen los nombres de campos y sus propiedades.

Se definen los datos de entrada y de respuesta, donde **type** son objetos de salida e **input** son objetos de entrada.

##### queries

Aquí se definen consultas (queries).

Agregar, editar, eliminar son Mutations.

##### resolvers

Aquí se define la interacción con la base de datos.

