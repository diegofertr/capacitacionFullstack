# Graphql #

La idea de graphql es filtrar las consultas.

*localhost:3000/graphiql* para consultar y armar consultas graphql que está instalado en el backend del proyecto.

Instalar la herramienta para **tokens de autenticación** -> **ModHeader** en el navegador para graphql.

Graphql maneja dos cosas principalmente que son queries y mutations.

## Ejmplos Queries

Usando `localhost:3000/graphiql` el editor autocompleta para escribir las consultas.

### Ejemplos Mutations

Usando **graphiql** el editor autocompleta para escribir los mutations donde se antepone la palabra *mutation*.

## schemes

En el backend en la carpeta `graphql/schemes` hay archivos donde se define que campos devuelve cada modelo, por ejemplo:

module.exports = `
	aqui va el codigo de grapql
	`
Digamos para un módulo Test:

module.exports = `
type Test {
  id: ID
  nombre: String
  descripcion: String
  fecha: Date
  estado: 
}

# aqui se esta definiendo un nuevo tipo en este caso enum
enum EstadoTest {
  ACTIVO
  INACTIVO
  }

# este es el tipo input (que es lo que quiero recibir)
input NuevoTest {
  nombre: String
  descripcion: String
  fecha: Date
  estado: EstadoTest
  mensaje: String
}

# este es un objeto de tipo salida (lo que quiero enviar como respuesta)
type Tests {
  count: Int
  rows: [Test]  # esto es un array 
}
`

En graphql al introducir comentarios (#) se autogenera la documentación.

Para el proyecto

## Queries

En `application/graphql/queries`, **todo lo que se va a devolver o consultar siempre debe estar definido en schemes**.

Se definen queries como objetos, tienen que ser un objeto `Query` y un objeto `Mutations`, por ejemplo:

module.exports = {
  Query: `
    tests(
	  limit: int
	  page: Int
	  order: String
    ): Tests
    # al decir tests(): Tests --> lo que esta luego de : indica lo que se va a devolver (ya estaba definido en Schemes
  `,
  Mutation: `
    agregarTest(test: NuevoTest) : Test
    # en el caso anterior usamos  'NuevoTest' que ya estaba definido para recibir esos parametros definidos en NuevoTest y hay que almacenarlo en un tipo en este caso como variable por eso va 'test: NuevoTest'.
  `
}

## Resolvers

Hace la interaccion entre graphql y la aplicacion. Para esto se puede usar lo que se ha hecho ya en `api/` y reutilizarla.

(Revisar ejemplos en `resolvers/`)

Por ejemplo para definir parametros en los `utils` de las consultas que hace `sequelize` hay que escribir codigo para tomar a esos parametros.

Para parametrizar las consultas (no quemar variables) en *graphiql* se puede escribir

query listaDeTest ($limite: Int, $orden: String) {
  tests( limit: $limite, order: $orden) {
	# aqui la consulta graphql
  }
}

Cuando se coloca argumentos aparece en *graphiql* una seccion QUERY VARIABLES donde se introducen objetos json parametrizando.

## Mutations

Se define en `resolvers/` (revisar los ejemplos en `resolvers/`).

En *graphiql* también se pueden escribir mutations para consultar, por ejemplo pasando argumentos:

mutation agregar($objeto: NuevoTest) {
  agregarTest(test: $objeto) {
	id
	nombre
  }
}

## Campos obligatorios

Para decirle a graphql que los campos sean obligatorios se coloca `!` en `schemes/`

## Consumir desde el frontend

En el proyecto en que esta hecho con `Vue.js` hay una herramienta llamada `apollo-client` para hacer consultas graphql pero esta optimizado para graphql y en el caso de vue se está usando `axios`.

En axios se maneja un solo endpoint en este caso `http://localhost:3000/graphql` y en los componentes de Vue se agrega el metodo: `axios.post()` (ver ejemplos en `src/common/plugins/service.js` en el frontend)

## Volviendo al backend

## application/graphql/server.js

Se usa `graphql-server-express` y `graphql-tools`.
Hay una varaible con `rootQuery` donde se cargan mutations y queries definidas en schemas de graphql. Ahí se concatenan los schemas con los queries.

En este archivo se crea el endpoint para graphql y también se configura para formatear el error. Finalmente se le pasa un `context` que se define más arriba.

Luego se habilita *graphiql* (graphiql esta incluido en para `graphql-server-express`) para poder usarlo en modo desarrollo.

## application/graphql/index.js

Se cargan los resolvers, schemas y queries definidos para graphql.




